FROM quay.io/andrej/novnc-gui-container:0.1.6

LABEL maintainer="Andrej Radović (r.andrej@gmail.com)"

ENV APP_NAME="SoulseekQt"
COPY vncmain.sh /app/vncmain.sh

ADD https://www.slsknet.org/SoulseekQt/Linux/SoulseekQt-2018-1-30-64bit-appimage.tgz \
    /tmp/soulseek.tgz

RUN apt-get update -y && \
    apt-get install --no-install-recommends -y \
    xterm \
    tar \
    binutils \
    dbus \
    && \
    rm -rf /var/lib/apt/lists/*

RUN tar -xvzf /tmp/soulseek.tgz -C /tmp && \
    /tmp/SoulseekQt-2018-1-30-64bit.AppImage --appimage-extract && \
    mv /squashfs-root/* /app && \
    strip /app/SoulseekQt

VOLUME /data/.SoulseekQt
VOLUME /data/downloads
VOLUME /data/logs
VOLUME /data/shared

# to account for ansible-podman's inability to have spaces in mounts
RUN ln -s /data/downloads /data/Soulseek\ Downloads && \
    ln -s /data/logs /data/Soulseek\ Chat\ Logs && \
    ln -s /data/shared /data/Shared\ Files
