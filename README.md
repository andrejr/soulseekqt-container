# soulseekqt-container

A simple image to wrap SoulseekQt in a (Docker/Podman) container, allowing you
to access its GUI via the browser.

The web server is on port 9000.

This container is supposed to be used rootlessly: the program runs as root
inside the container, which means that the files created by it are given the
UID:GID combination of the user that's running the container rootlessly.
Don't use it otherwise.

The program window is resized with the browser window, which makes for a
native-like experience.

## Usage

Just mount your directories and run it.
Don't forget to share `/data/Shared Files` in the GUI.

Here's an example:
```
podman run --rm -it \
    -p 9000:9000 \
    -v "/persistent/ssqt/appdata":"/data/.SoulseekQt" \
    -v "/persistent/ssqt/downloads":"/data/downloads" \
    -v "/persistent/ssqt/logs":"/data/logs" \
    -v "/mnt/media/music":"/data/shared" \
    quay.io/andrej/soulseekqt-container:latest
```
